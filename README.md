# TP1 de programmation Web

## Mise en route

Installation des dépendances :
``
npm install
```

Lancement du serveur :
```
node server.js
```

Le serveur est alors accessible à l’adresse http://localhost:1234

## Objectif

### HTML

* Créer le fichier « `index.html` » dans le dossier « `public` »,
* recharger la page et vérifier que le contenu correspond,
* ajouter un formulaire à la page HTML,
* recharger la page,
* remplir le formulaire,
* vérifier que le contenu du formulaire est visible dans les logs du serveur.

### CSS

* Créer un fichier « `styles.css` » dans le dossier « `public` »,
* charger la feuille de styles dans la page HTML,
* définir des styles pour la balise « `h1` ».

### JavaScript

* Créer un fichier « `main.js` » dans le dossier « `public` »,
* afficher le contenu de la balise « `h1` » sur la console JavaScript.

## Licence

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
